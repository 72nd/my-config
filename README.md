# My dotfiles and setup scripts

As the title says.


## First steps

```bash
sudo dnf install git
```


## Setup

The setup portion uses the task runner [Task](https://taskfile.dev/#/). You first have to run `setup/bootstrap.sh` in order to get the environment. Please note that the bootstrap script will only work on amd64, Fedora systems.

You then can run the setup scripts.

```bash
task -t setup/FILE.yml
```
