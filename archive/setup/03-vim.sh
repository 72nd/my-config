#!/bin/sh

echo "Step 3: install and configure neo-vim"

# Package Manager
mkdir ~/.chache/dein
curl https://raw.githubusercontent.com/Shougo/dein.vim/master/bin/installer.sh > /tmp/dein-installer.sh
sh /tmp/dein-installer.sh ~/.cache/dein

# Config
scitation=$(realpath ../config/citation.vim)
tcitation=$HOME/.config/nvim/citation.vim
sinit=$(realpath ../config/init.vim)
tinit=$HOME/.config/nvim/init.vim

mkdir $HOME/.config/nvim
ln -s $scitation $tcitation 
ln -s $sinit $tinit

echo "Will now open nvim to install dein, please close nvim after the installation"
read -s
nvim -c 'call dein#install()'

echo "Will now open nvim to update dein, please close nvim after this"
read -s
nvim -c 'call dein#update()'
