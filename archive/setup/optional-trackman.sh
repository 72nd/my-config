#!/bin/bash

echo "Optional Step: configure Logitech Trackman for scrolling"
sudo echo -e "Section \"InputClass\"\n\
\tIdentifier  \"Marble Mouse\"\n\
\tMatchProduct \"Logitech USB Trackball\"\n\
\tDriver \"libinput\"\n\
\tOption \"ScrollMethod\" \"button\"\n\
\tOption \"ScrollButton\" \"8\"\n\
EndSection" >> /usr/share/X11/xorg.conf.d/40-libinput.conf

echo "About to logout. Login to apply changes"
echo "Press enter to proceed with logout"
read -s
logout
