#!/bin/sh

echo "Step 2: Terminal programs I need"
sudo apt update

sudo apt install neovim ubuntu-dev-tools tmux bat curl fd-find ffmpeg fzf jq exfat-fuse exfat-utils

# Tmux
echo 'set -g default-terminal "screen-256color"' > $HOME/.tmux.conf
echo 'set -g mouse on' >> $HOME/.tmux.conf
tmux source-file ~/.tmux.conf

# fd-find
echo "alias fd=fdfind" >> $HOME/.zshenv

