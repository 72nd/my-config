#!/bin/sh

echo "Step 1: Usable UI"
dconf write /org/gnome/desktop/sound/event-sounds false

echo "Select the «gnome-shell.css» with no ubuntu name in the path"
sudo update-alternatives --config gdm3.css 

sudo apt install gnome-tweak-tool gnome-session arc-theme gnome-shell-extensions

echo "About to reboot. At login choose GNOME session, login, change to arc-dark theme in tweak tools"
echo "Press enter to proceed with reboot"
read -s
sudo shutdown -r 0
