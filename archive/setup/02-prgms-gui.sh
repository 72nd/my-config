#!/bin/sh

echo "Step 2: GUI programs I need"
sudo add-apt-repository ppa:nextcloud-devs/client
sudo apt update

sudo apt install neovim-qt evolution nemo nextcloud-client nextcloud-client-nemo audacity telegram-desktop vlc keepassx chrome-gnome-shell gir1.2-gtop-2.0 gir1.2-nm-1.0 gir1.2-clutter-1.0 

# Nemo
xdg-mime default nemo.desktop inode/directory application/x-gnome-saved-search

# system-monitor
echo "please install the system-monitor plugin in your browser, press enter to continue"
read -s
firefox https://extensions.gnome.org/extension/120/system-monitor/ 
