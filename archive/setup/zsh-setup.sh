#!/bin/sh

# ZSH
sudo apt install zsh wget git

# ZSH as default

chsh -s $(which zsh)

# Oh my ZSH
RUNZSH=no sh -c "$(wget -O- https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# Powerlevel10k
git clone https://github.com/romkatv/powerlevel10k.git ~/.oh-my-zsh/themes/powerlevel10k
sed -i "/ZSH_THEME=\".*\"/c\ZSH_THEME=\"powerlevel10k/powerlevel10k\"" ~/.zshrc

# Fonts
wget -O /tmp/meslo_lgs_nf_regular.ttf https://github.com/romkatv/dotfiles-public/raw/master/.local/share/fonts/NerdFonts/MesloLGS%20NF%20Regular.ttf
wget -O /tmp/meslo_lgs_nf_bold.ttf https://github.com/romkatv/dotfiles-public/raw/master/.local/share/fonts/NerdFonts/MesloLGS%20NF%20Bold.ttf
wget -O /tmp/meslo_lgs_nf_italic.ttf https://github.com/romkatv/dotfiles-public/raw/master/.local/share/fonts/NerdFonts/MesloLGS%20NF%20Italic.ttf
wget -O /tmp/meslo_lgs_nf_bold-italic.ttf https://github.com/romkatv/dotfiles-public/raw/master/.local/share/fonts/NerdFonts/MesloLGS%20NF%20Bold%20Italic.ttf

FONTDIR="/usr/share/fonts/truetype/meslo/"
sudo mkdir $FONTDIR
sudo mv /tmp/meslo_lgs_nf_regular.ttf $FONTDIR
sudo mv /tmp/meslo_lgs_nf_bold.ttf $FONTDIR
sudo mv /tmp/meslo_lgs_nf_italic.ttf $FONTDIR
sudo mv /tmp/meslo_lgs_nf_bold-italic.ttf $FONTDIR
sudo fc-cache -f -v

echo "Please set Meslo LGS NF as your terminal font"
echo "Then log out and in again to apply the changes"
echo "Then run \"p10k configure\""
