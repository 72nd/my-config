# My vim config − The readme

## Commands

| Keys              | Description                        |
|-------------------+------------------------------------|
| **General**       |                                    |
| `ö`               | Beginning of sentence              |
| `ä`               | End of sentence                    |
| `ü`               | Begininng of paragraph             |
|                   |                                    |
| **CtrlSpace**     |                                    |
| `Leader + Leader` | CtrlSpace                          |
| `Leader + f`      | CtrlSpace search for file          |
| `Leader + b`      | CtrlSpace search buffers           |
| `Leader + l`      | CtrlSpace tab list                 |
|                   |                                    |
| **Functions**     |                                    |
| `Leader + o`      | Toggle line wrapping               |
| `Leader + c + s`  | Show synonyms for word             |
|                   |                                    |
| **Documents**     |                                    |
| `Leader + e + s`  | Execute gosmooth                   |
| `Leader + e + o`  | Open PDF based on current md file  |
| `Leader + e + c`  | Execute word-count and show result |
| `Leader + p + c`  | Opens citation utility             |


