set termguicolors

if &compatible
    set nocompatible
endif
" Add the dein installation directory into runtimepath
set runtimepath+=~/.cache/dein/repos/github.com/Shougo/dein.vim

" Dear me: please state for each plugin, why I need it.
if dein#load_state('~/.cache/dein')
  call dein#begin('~/.cache/dein')
  " Plugin/Package Manager
  call dein#add('~/.cache/dein/repos/github.com/Shougo/dein.vim')
  " Completion
  call dein#add('Shougo/deoplete.nvim')
  " Theme
  call dein#add('rakr/vim-one')
  " File tree view
  call dein#add('scrooloose/nerdtree')
  " Pandoc
  call dein#add('vim-pandoc/vim-pandoc')
  " More Pandoc
  call dein#add('vim-pandoc/vim-pandoc-syntax')
  " Statusline
  call dein#add('vim-airline/vim-airline')
  " Tables in markdown etc.
  call dein#add('dhruvasagar/vim-table-mode')
  " Additional spell checking
  call dein#add('rhysd/vim-grammarous')
  " A lot of fun with surroundings
  call dein#add('tpope/vim-surround')
  " Git Integration
  call dein#add('tpope/vim-fugitive')
  " Query for synonyms online
  call dein#add('ron89/thesaurus_query.vim')
  " For the textobj-quote package
  call dein#add('kana/vim-textobj-user')
  " Swiss quotes
  call dein#add('reedes/vim-textobj-quote')
  " Navigtion and more
  call dein#add('vim-ctrlspace/vim-ctrlspace')
  " Fast navigation in the text
  " call dein#add('easymotion/vim-easymotion')
  " Syntax highlighting for the twig templating language
  call dein#add('nelsyeung/twig.vim')
  " Language Server
  call dein#add('autozimu/LanguageClient-neovim', {
              \ 'rev': 'next',
              \ 'build': 'bash install.sh',
              \ })
  " Toml syntax highlighting
  call dein#add('cespare/vim-toml')
  " Search, duh
  call dein#add('junegunn/fzf', { 'build': './install --all', 'merged': 0 }) 
  call dein#add('junegunn/fzf.vim', { 'depends': 'fzf' })
  " Delve, a golang debugger
  call dein#add('sebdah/vim-delve')
  " Some stuff for working with ledger/hledger files
  call dein#add('ledger/vim-ledger')
  " Jinja templating language syntax highlighting.
  call dein#add('Glench/Vim-Jinja2-Syntax')
  if !has('nvim')
      " call dein#add('roxma/nvim-yarp')
  endif

  call dein#end()
  call dein#save_state()
endif


" Shell
set shell=zsh

filetype plugin indent on
filetype plugin on
syntax enable

runtime citation.vim

" Is Word
set iskeyword-=_

" Quotes
" augroup textobj_quote
    " autocmd!
    " autocmd FileType markdown call textobj#quote#init()
    " autocmd FileType textile call textobj#quote#init()
" augroup END

" let g:textobj#quote#doubleDefault = '«»'
" let g:textobj#quote#singleDefault = '‹›'

" Apperance
colorscheme one
let g:airline_theme='one' 
set background=dark
set number
set mouse=a

" General remapping
nnoremap ü +
nnoremap ö (
nnoremap ä )
nnoremap z0 z=

" Insert mode remapping
inoremap <C-l> <C-o>O

" Markdown
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_frontmatter = 1
let g:pandoc#folding#level = 5

" Grammar, Synonyms, Spellchecking
set spell spelllang=de_ch
set spellfile=~/repos/my-config/spelling/de.utf-8.add

" let g:grammarous#use_vim_spelllang
autocmd BufRead,BufNewFile *.md setlocal spell

let g:tq_language=['de']
let g:tq_enable_backends=["woxikon_de", "openthesaurus_de"]
let g:tq_online_backends_timeout = 3.0

" Table Mode
function! s:isAtStartOfLine(mapping)
    let text_before_cursor = getline('.')[0 : col('.')-1]
    let mapping_pattern = '\V' . escape(a:mapping, '\')
    let comment_pattern = '\V' . escape(substitute(&l:commentstring, '%s.*$', '', ''), '\')
    return (text_before_cursor =~? '^' . ('\v(' . comment_pattern . '\v)?') . '\s*\v' . mapping_pattern . '\v$')
endfunction

inoreabbrev <expr> <bar><bar>
            \ <SID>isAtStartOfLine('\|\|') ?
            \ '<c-o>:TableModeEnable<cr><bar><space><bar><left><left>' : '<bar><bar>'
inoreabbrev <expr> __
            \ <SID>isAtStartOfLine('__') ?
            \ '<c-o>:silent! TableModeDisable<cr>' : '__'

" Autodir
" set autochdir

" Font
set guifont=MesloLGS\ NF:h11

" Clipboard
" set clipboard+=unnamedplus
" let g:loaded_clipboard_provider = 1
" let g:loaded_clipboard_provider = "xclip"

" Leader
:let mapleader = "\<Space>"

" CTRL SPACE
set hidden
set tabline=0
let g:CtrlSpaceDefaultMappingKey = "<Leader><Leader> "
if executable("ag")
    let g:CtrlSpaceGlobCommand = 'ag -l --nocolor -g ""'
endif

let g:CtrlSpaceLoadLastWorkspaceOnStart = 1
let g:CtrlSpaceSaveWorkspaceOnSwitch = 1
let g:CtrlSpaceSaveWorkspaceOnExit = 1


nmap <Leader>f :Files<CR>
nmap <Leader>b :Buffers<CR>

" EASYMOTION
" map s <Plug>(easymotion-prefix)
" map  / <Plug>(easymotion-sn)
" omap / <Plug>(easymotion-tn)
" map  n <Plug>(easymotion-next)
" map  N <Plug>(easymotion-prev)

" DEOPLETE
call deoplete#custom#source('_', 'matchers', ['matcher_full_fuzzy'])
call deoplete#custom#option('smart_case', v:true)

let g:deoplete#enable_at_startup = 1
inoremap <expr> <C-j> pumvisible() ? "\<C-n>" : "\<C-j>"
inoremap <expr> <C-k> pumvisible() ? "\<C-p>" : "\<C-k>"

" LEDGER
let g:ledger_bin = 'hledger'

" LANGUAGE SERVER
augroup filetype_rust
    autocmd!
    autocmd BufReadPost *.rs setlocal filetype=rust
augroup END

let g:LanguageClient_serverCommands = {
            \ 'rust': ['~/.cargo/bin/rustup', 'run', 'stable', 'rls'],
            \ 'go': ['~/.local/go/bin/gopls'],
			\ 'python': ['~/.local/bin/pylsp'],
            \ }
autocmd FileType * call LanguageClientMaps()
let g:LanguageClient_loggingFile =  expand('~/.local/share/nvim/LanguageClient.log') 

function! LanguageClientMaps()
    if has_key(g:LanguageClient_serverCommands, &filetype)
        nnoremap <F5> :call LanguageClient_contextMenu()<CR>
        nnoremap <silent> K :call LanguageClient#textDocument_hover()<CR>
        nnoremap <buffer> <silent> gd :call LanguageClient#textDocument_definition()<CR>
        nnoremap <leader>lr :call LanguageClient#textDocument_rename()<CR>
        nnoremap <leader>lf :call LanguageClient#textDocument_formatting()<CR>
        nnoremap <leader>lt :call LanguageClient#textDocument_typeDefinition()<CR>
        nnoremap <leader>lx :call LanguageClient#textDocument_references()<CR>
        nnoremap <leader>la :call LanguageClient_workspace_applyEdit()<CR>
        nnoremap <leader>lc :call LanguageClient#textDocument_completion()<CR>
        nnoremap <leader>ls :call LanguageClient_textDocument_documentSymbol()<CR>
        nnoremap <leader>lm :call LanguageClient_contextMenu()<CR>
    endif
endfunction

" Vista
let g:vista_default_executive = 'lcn'
let g:vista#renderer#enable_icon = 1

" Golang
" autocmd FileType go setlocal tabstop=4

" COMMANDS
nmap <Leader>es :call RunGoSmooth()<CR>
function! RunGoSmooth()
    if &filetype != "pandoc"
        echom "Aborted, this isn't a markdown file."
        return
    endif
    write

    call system('rsmooth ' . expand('%') )
    echo "Rsmooth done."
endfunction

nmap <Leader>eo :call OpenPdfFromMd()<CR>
function! OpenPdfFromMd()
    if &filetype == "pandoc"
        let pdfPath = expand('%:r') . ".pdf"
        if filereadable(pdfPath)
            silent execute '!gio open ' . pdfPath
        else
            echom "There isn't a pdf for this md file"
        endif
    else
        echom "Not a pandoc file"
    endif
endfunction

nmap <Leader>ec :call MyWordCount()<CR>
function! MyWordCount()
    let result = system('word-count ' . expand('%'))
    echom expand('%:t') . ": " . trim(result) . " chars running text without spaces."
endfunction

nmap <silent> <Leader>pc :call Citation()<CR>
nmap <silent> <Leader>pp :call InsertCitationFromRegister()<CR>
imap <C-space>pc :call Citation()<ESC>

nnoremap <Leader>w <C-w>
nnoremap <Leader>el "+p

" ABBREVATIONS
iabbrev verb ```
iabbrev blb \[...\]
ab bib [i]


" Jinja2 Syntax for .tera files.
autocmd BufNewFile,BufRead *.html.tera set ft=jinja


" DIV
" set autoindent noexpandtab tabstop=4 shiftwidth=4
set tabstop=4 shiftwidth=4
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab


" Wrap long lines
" set wrap lbr
" noremap <silent> <Leader>u :call ToggleWrap()<CR>
" function! ToggleWrap()
"     if &wrap
"         setlocal nowrap
"         set virtualedit=all
"         silent! nunmap <buffer> <Up>
"         silent! nunmap <buffer> <Down>
"         silent! nunmap <buffer> <Home>
"         silent! nunmap <buffer> <End>
"         silent! iunmap <buffer> <Up>
"         silent! iunmap <buffer> <Down>
"         silent! iunmap <buffer> <Home>
"         silent! iunmap <buffer> <End>
"     else
"         setlocal wrap linebreak nolist
"         set virtualedit=
"         setlocal display+=lastline
"         noremap  <buffer> <silent> <Up>   gk
"         noremap  <buffer> <silent> <Down> gj
"         noremap  <buffer> <silent> <Home> g<Home>
"         noremap  <buffer> <silent> <End>  g<End>
"         noremap  <buffer> <silent> j gj
"         noremap  <buffer> <silent> k gk
"         inoremap <buffer> <silent> <Up>   <C-o>gk
"         inoremap <buffer> <silent> <Down> <C-o>gj
"         inoremap <buffer> <silent> <Home> <C-o>g<Home>
"         inoremap <buffer> <silent> <End>  <C-o>g<End>
"     endif
" endfunction


