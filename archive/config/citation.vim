function! Citation()
    let jsonPath = CitationGetCslJsonPath()
    if jsonPath == "0"
        return
    endif
    let searchTerm = CitationGetTerm()
    if searchTerm == ""
        return
    endif
    let results = CitationSearch(jsonPath, searchTerm)
    if len(results) == 0
        echom "No results found"
        return
    endif
    let id = CitationGetId(results)
    call InsertCitation(id)
endfunction

function! CitationGetCslJsonPath()
    let line = system("grep bibliography: " . expand("%"))
    if match(line, "bibliography:") == -1
        echom "No bibliography entry in front matter found"
        return "0"
    endif
    let filename = substitute(line, "[ ]*bibliography: ", "", "g")
    return expand('%:p:h') . "/" . filename
endfunction

function! CitationGetTerm()
    let result = input("Enter search term: ")
    redraw
    return result
endfunction

function! CitationSearch(jsonPath, searchTerm)
    let rawResult = system("csl-id -i " . trim(a:jsonPath) . " -t " . a:searchTerm)
    let results = []
    for raw in split(rawResult, "\n")
        let results = results + [split(raw, "|")]
    endfor
    return results
endfunction

function! CitationGetId(results)
    let i = 1
    echo "Choose entry:\n"
    for result in a:results
        echo i . ") " . result[1] . " " . result[2] "\n"
        let i = i + 1
    endfor

    let selection = str2nr(input(""))
    redraw
    if selection == 0 || selection > len(a:results)
        echom "Not a valid choice"
        return
    endif
    return a:results[selection - 1][0]
endfunction

function! InsertCitation(id)
    let citeNormal = "[@" . a:id . "]"
    let citeVgl = "[Vgl.: @" . a:id . "]"
    let citeAppend = ";@" . a:id
    echo "Choose required ciation format:\na) " . citeNormal . "\nb) " . citeVgl . "\nc) " . citeAppend 
    let mode = nr2char(getchar())
    redraw
    if mode == "a"
        execute "normal! a" . citeNormal . "\<ESC>"
    elseif mode == "b"
        execute "normal! a" . citeVgl . "\<ESC>"
    elseif mode == "c"
        execute "normal! bt]a" . citeAppend . "\<ESC>"
    elseif mode == "\<ESC>"
    else
        echom "Choice not valid."
    endif
endfunction

function! InsertCitationFromRegister()
    let clipboard = @"
    InsertCitation(clipboard)
endfunction
