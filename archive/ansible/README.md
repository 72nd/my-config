# 72nd Ansible Stuff

```shell script
apt install git ansible 
```

## Config

### Mail

```yml
mail_accounts:
    - name: personal
      display_name: Personal
      from_address: "my.name@providor.com"
      from_name: Some Name
      user: "my.name@provider.com"
      password: "secret"
      imap_host: "imap.provider.com"
      imap_port: 993
      folders: # optional, if not defined all folders will be synchronized
        - INBOX
        - Sent
        - Trash
      smtp_host: "smtp.provider.com"
      smtp_port: 465
      smtp_auth: tls # tls, None
```

