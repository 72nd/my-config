# Deploy some executable as systemd service

## Preparations

First, create a new user:

```shell
adduser --system --shell /bin/bash --gecos 'FUNCTION_OF_USER' --group --disabled-password SERVICE_USER_NAME
```

Then create the directories:

```shell
mkdir /usr/local/bin/SERVICE_NAME # Location of the binary etc.
mkdir /var/lib/SERVICE_NAME # Working dir
chown -R SERVICE_USER:SERVICE_USER /var/lib/SERVICE_NAME
chmod -R 750 /var/lib/SERVICE_NAME
```

Copy all necessary files to `/usr/local/bin/SERVICE_NAME` and then:

```shell
chown -R SERVICE_USER:SERVICE_USER /usr/local/bin/SERVICE_NAME
chmod -R 750 /usr/local/bin/SERVICE_NAME
```


## Service

Services are defined in `/etc/systemd/system/`. Therefore create a new file `/etc/systemd/system/SERVICE_NAME.service` with the following content:

```
[Unit]
Description=DESCRIPTION_OF_THE_SERVICE
After=syslog.target network.target

[Service]
Type=simple
User=SERVICE_USER
Group=SERVICE_USER
WorkingDirectory=/var/lib/SERVICE_NAME
Environment= NEEDED_ENVIROMENT_VARIABLES
ExecStart=START_COMMAND_FOR_SERVICE
Restart=always

[Install]
WantedBy=multi-user.target
```

Then enable and start the service.
