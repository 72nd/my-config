# DB-Stuff

## Postgres

Install and start:

```bash
sudo apt install postgresql postgresql-contrib libpq-dev
sudo service postgresql start
```

Enter Postgres shell:

```bash
sudo -i -u postgres psql
```

Create new user and add a new DB for this user:

```postgres
CREATE USER [user_name] WITH PASSWORD '[password]';
CREATE DATABASE [dn_name] OWNER [user_name];
\q
```

Other commands (also see this [cheat sheet](https://gist.github.com/Kartones/dd3ff5ec5ea238d4c546)):

- List all databases: `\l` or `\list`
- Switching database: `use [db_name]` or `\connect [db_name]` or `\c [db_name]`
- Listing tables: `\dt`
- Show table definition (columns etc.): `\d [table_name]`
- Show table content: `TABLE [table_name];`
- List users: `\du`


## MySQL

Add new user and db, short version:
```bash
mysql -u root -p
> CREATE DATABASE [db_name]; 
> CREATE USER '[user_name]'@'localhost' IDENTIFIED BY '[password]';
> GRANT ALL PRIVILEGES ON [db_name] . * TO '[user_name]'@'localhost';
```

Enter MySQL shell:

```bash
mysql -u root -p
```

Show all DBs:

```sql
SHOW DATABASES;
```

Create new DB:

```sql

CREATE DATABASE [db_name];
```

Delete DB:

```sql
DROP DATABASE [db_name];
```

Create User and grant privileges:
```sql
CREATE USER '[user_name]'@'localhost' IDENTIFIED BY '[password]';
GRANT ALL PRIVILEGES ON [db_name] . * TO '[user_name]'@'localhost';
```


