# Server installations guide (Linux, sshd, dnsmasq, data on encrypted RAID 1, Nextcloud, gitea, and more...)

This document contains some help for repetitively tasks in the field of Linux server administration. There is by far no claim to completeness. I see it more as a personal collection of receipts and mnemonics. Do not use this as your single source of information..


## RAID & Encryption

This will create an encrypted RAID Level 1 on `/dev/sda` and `dev/sdb` for external storage. Remark: This is not applicable for system partitions. Based on [this ArchWiki page](https://wiki.archlinux.org/index.php/Dm-crypt/Encrypting_an_entire_system#LUKS_on_software_RAID). Find more information about software RAID also in the [ArchWiki](https://wiki.archlinux.org/index.php/RAID).

```shell
apt install cryptsetup mdadm
```


### Erase disks

More information: [ArchWiki: Drive preparation](https://wiki.archlinux.org/index.php/Dm-crypt/Drive_preparation). 

```
cryptsetup open --type plain -d /dev/urandom /dev/sda to-be-wiped-a
cryptsetup open --type plain -d /dev/urandom /dev/sdb to-be-wiped-b
```

Use `lsblk` to check new configuration then do this over night on a fast system (will take some time):

```
dd if=/dev/zero of=/dev/mapper/to-be-wiped-a status=progress
dd if=/dev/zero of=/dev/mapper/to-be-wiped-b status=progress
```

When done:

```
cryptsetup close to-be-wiped-a
cryptsetup close to-be-wiped-b
```


### Format disks

In general `lsblk` and `blkid` (the latter needs root privileges) are your friends. `<CR>` refers to the Enter key.

Unplug both drives and plug one in again.

```sh
fdisk /dev/sda
> g         # create new partition table
> n         # add a new partition
  > 1       # set partition number to 1
  > <CR>    # set default start sector
  > <CR>    # set default end sector
> t         # change partition type
  > ...     # set partition to type "Linux RAID"
> x         # enter expert mode
> n         # set the partition lable (ex.: "storage-a")
> r         # return to normal mode
> w         # apply changes and quit
```

Unplug the formatted device, plug the other in and repeat the whole section for this drive. **It's important, you set the label of the second drive to another name than the first drive!**

When you're done with the second drive unplug it and plug both drives back in again. Wait with the insertion of the second drive until the first available to the system. Then check the availability of both devices:

```
ls -l /dev/disk/by-partlabel/
```


### Create RAID

Now you can create the RAID.

```sh
mdadm --create --verbose --level=1 --metadata=1.2 --raid-devices=2 /dev/md/ARRAY_NAME /dev/disk/by-partlabel/storage-a /dev/disk/by-partlabel/storage-b
```


### Cryptsetup

```sh
cryptsetup -y -v luksFormat --type luks1 /dev/md/ARRAY_NAME
cryptsetup open /dev/md/ARRAY_NAME CRYPT_ROOT_NAME
mkfs.ext4 /dev/mapper/CRYPT_ROOT_NAME
mount /dev/mapper/CRYPT_ROOT_NAME /mnt
```

Save a script to your home folder with the following content:

```
cryptsetup open /dev/md/ARRAY_NAME CRYPT_ROOT_NAME
mount /dev/mapper/CRYPT_ROOT_NAME /mnt
```


## Tools

### zsh

Install dependencies.

```shell
sudo apt install zsh wget git
```

Set zsh as default shell.

```shell
chsh -s $(which zsh

# On Fedora systems without chsh
lchsh -i USER_ID
```

Oh my zsh and Powerlevel10k theme 

```shell
RUNZSH=no sh -c "$(wget -O- https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
git clone https://github.com/romkatv/powerlevel10k.git ~/.oh-my-zsh/themes/powerlevel10k
```

Terminate SSH-session and login again to start the p10k config assistant. If necessary the assistant can also be started with `p10k configure`.

## Webservices dependencies 

### General

### Firewall ufw

For Ubuntu/Debian based systems.

```shell
apt install ufw
ufw allow 22
ufw allow 80
ufw allow 443
ufw enable
```


### Firewall firewalld

For Fedora/RHEL based systems.

Installation.

```shell
dnf install firewalld
systemctl enable firewalld
systemctl start firewalld
```

Get current state.

```shell
firewall-cmd --state
firewall-cmd --get-active-zones
firewall-cmd --list-services
```

Change stuff. Omit the `--permanent` flag if you only do some temporary changes (they will be dropped on reload).

```shell
# Change Interface
firewall-cmd --change-interface=NETWORK_INTERFACE --zone=ZONE --permanent

# Open Ports
firewall-cmd --add-service=http --add-service=https --permanent
firewall-cmd --add-port=67/udp --permanent

# Close Ports, --zone can be omitted if default
firewall-cmd --zone=public --remove-port=5000/tcp --permanent
firewall-cmd --remove-service=http --permanent

firewall-cmd --reload
``` 

### fail2ban

```shell
apt install fail2ban
cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
service fail2ban restart
```

### dnsmasq (if needed)

Taken from [here](https://computingforgeeks.com/install-and-configure-dnsmasq-on-ubuntu-18-04-lts/) and [here](https://wiki.ubuntuusers.de/Dnsmasq/).

#### Pre configuration

Disable systemd-resolve.

```shell
systemctl disable systemd-resolved
systemctl stop systemd-resolved
```

Remove resolv.conf.

```shell
rm /etc/resolv.conf
```

Create new `/etc/resolv.conf` with the following content:

```
nameserver 127.0.0.1
nameserver 1.0.0.1
```

If the package `resolvconf` is installed, the `/etc/resolv.conf` will always be overwritten when dnsmasq starts. To suppress this add `resolvconf=NO` to `/etc/resolvconf.conf`.

#### Installation

```shell
apt install dnsmasq
```


#### Configuration

Edit `/etc/dnsmasq.conf` and replace `XXX.XXX.XXX.XXX` with the local IP of your server.

```
port=53
domain-needed
bogus-priv
strict-order
server=1.0.0.1
listen-address=127.0.0.1 
listen-address=XXX.XXX.XXX.XXX
```

Custom DNS records can be added to `/etc/dnsmasq.conf` as follows:

```
address=/YOUR.DOMAIN.COM/XXX.XXX.XXX.XXX
```

Restart dnsmasq.

```shell
systemctl restart dnsmasq
```

If using ufw:

```shell
ufw allow 53
```

Then restart dnsmasq. It's possible you have to rewrite the content of the `resolv.conf` and then restart dnsmasq again.

#### Testing

Make sure, dnsmasq listens correctly on 127.0.0.1 (needs root privileges):

```shell
netstat -tulpen | grep dnsmasq
```

For further testing you have to install `dnsutils`:

```shell
apt install dnsutils
```

Then:

```shell
# general testing, local
dig stgtk.ch @local

# general testing
dig stgtk.ch

# from another machine
dig @XXX.XXX.XXX.XXX stgtk.ch
```

If you've set some custom DNS records test them also and make sure, they return their custom set value.


### Access from the internet

1. Go to [duckdns.org](https://www.duckdns.org) and login.
2. Add a new domain
3. Follow the [installation instructions](https://www.duckdns.org/install.jsp)
4. Create the needed CNAME Records in your DNS records and relay them to your chosen Duck DNS address


## PostgreSQL

Installation:

```shell
sudo apt install postgresql libpq-dev postgresql-client postgresql-client-common
```

Start:

```shell
service postgresql start
```

Create db:

```shell
sudo -u postgres createdb DB_NAME

# Also possible:
sudo -u postgres psql
> create database DB_NAME;
```

Create user:

```shell
sudo -u postgres psql
> create user USER with encrypted password 'PASSWORD';

# grant privileges on db
> grant all privileges on database DB_NAME to USER ;
```

Optional: [change data directory](https://www.digitalocean.com/community/tutorials/how-to-move-a-postgresql-data-directory-to-a-new-location-on-ubuntu-18-04)


## Simple nginx reverse-proxy (CNAME)

```shell
dnf install nginx letsencrypt python-certbot-nginx
systemctl enable nginx
systemctl start nginx
```

If you not on Ubuntu it's likely there is no `sites-available`/`sites-enabled` structure.

```shell
mkdir /etc/nginx/sites-available
mkdir /etc/nginx/sites-enabled
```

Then open the `/etc/nginx/nginx.conf` and add the following line inside the `http` block.

```nginx
include /etc/nginx/sites-enabled/*;
```

Create a service file `/etc/nginx/sites-available/SERVICE.conf`

```nginx
server {
	listen 80;
	listen [::]:80;
	server_name YOUR.CNAME.URL;

	access_log /var/log/nginx/SERVICE-access.log;
	error_log /var/log/nginx/SERVICE-error.log;

	location / {
		proxy_pass http://127.0.0.1:PORT;
	}
}
```

Enable configuration.

```shell
ln -s /etc/nginx/sites-available/SERVICE.confg /etc/nginx/sites-enabled/SERVICE.confg
```

Test configuration.

```shell
nginx -t
```

Reload nginx.

```shell
nginx -s reload
```

Get certificate.

```shell
certbot --nginx
```

## Nextcloud and nginx

### Installation 

```shell
apt install nginx php7.3 php7.3-fpm php7.3-gd sqlite php7.3-sqlite3 php7.3-curl php7.3-zip php7.3-xml php7.3-mbstring php7.3-pgsql php7.3-bz2 php7.3-intl php7.3-apcu
```

```shell
systemctl enable nginx
systemctl start nginx
```

### PHP

Configure in `/etc/php/7.3/fpm/php.ini`:

```
date.timezone = Europe/Berlin
cgi.fix_pathinfo=0
```

And uncomment in `/etc/php/7.3/fpm/pool.d/www.conf`:

```
env[HOSTNAME] = $HOSTNAME
env[PATH] = /usr/local/bin:/usr/bin:/bin
env[TMP] = /tmp
env[TMPDIR] = /tmp
env[TEMP] = /tmp
```

Uncomment the following lines in `/etc/nginx/sites-available/default`:

```nginx
location ~ \.php$ {
        try_files $uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        # NOTE: You should have "cgi.fix_pathinfo = 0;" in php.ini

        # With php5-cgi alone:
        #fastcgi_pass 127.0.0.1:9000; # <-- DO NOT UNCOMMENT THIS ONE
        # With php5-fpm:
        fastcgi_pass unix:/var/run/php7.3-fpm.sock; # <-- CHANGE PHP VERSION ACCORDING TO YOUR SYSTEM
        fastcgi_index index.php;
        include fastcgi_params;
}
```

```shell
systemctl enable php7.3-fpm
systemctl restart php7.3-fpm
```

### Download Nextcloud

```shell
wget https://download.nextcloud.com/server/releases/nextcloud-18.X.X.zip
unzip nextcloud-18.X.X.zip
mv nextcloud /PATH/TO/NEXTCLOUD
chown -R www-data:www-data /PATH/TO/NEXTCLOUD
```

### Config

### Let's encrypt

```shell
apt install install certbot python-certbot-nginx
```

And

```shell
cerbot --nginx
systemctl restart nginx
```



## Nextcloud and apache

Installation:

```shell
# Raspbian:
apt install apache2 php7.3 php7.3-gd sqlite php7.3-sqlite3 php7.3-curl php7.3-zip php7.3-xml php7.3-mbstring php7.3-pgsql php7.3bz2 php7.3-intl php7.3-apcu
```

Enable modules:

```shell
a2enmod rewrite
a2enmod headers
a2enmod env
a2enmod dir
a2enmod mime
a2enmod ssl
a2ensite default-ssl
```

Create new host under `/etc/apache2/sites-available/SUB-HOST-com.conf`:

```apache
<VirtualHost *:80>
    DocumentRoot /var/www/html/nextcloud/
    ServerName SUB.HOST.com
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
    RewriteEngine on
    RewriteCond %{SERVER_NAME} =SUB.HOST.com
    RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=prermanent]

    <Directory /var/www/nextcloud/>
        Require all granted
        AllowOverride All
        Options FollowSymLinks MultiViews
        <IfModule mod_dav.c>
          Dav off
        </IfModule>
    </Directory>
</VirtualHost>
```

Enable site and restart Apache:

```shell
a2ensite SUB-HOST-com
service apache2 restart
```

Visit the [certbot instruction site](https://certbot.eff.org/instructions) to get the commands according to your system.

```shell
# Debian/Raspbian
apt install certbot python-certbot-apache
certbot --apache
```


### Get Files

Go to the [Nextcloud download page](https://nextcloud.com/install/#instructions-server) and copy the current URL to the installation ZIP.

```shell
wget ZIP_URL -P /tmp
unzip /tmp/nextcloud-XX.X.X.zip -d /var/www/html
chown -R www-data:www-data /var/www/html/nextcloud/
```

Create/choose your Nextcloud data folder. Then:

```shell
chown www-data:www-data DATA_FOLDER
chmod 750 DATA_FOLDER
```


### Config DB



## Borg Backup

```shell
apt install borgbackup
```
