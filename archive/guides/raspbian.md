# Raspian Stuff

## Preparations

Connect the SD Card to your system.


### Wipe the content of the Card (optional)

```shell
cryptsetup open --type plain -d /dev/urandom /dev/SD_CARD to-be-wiped
dd if=/dev/zero of=/dev/mapper/to-be-wiped status=progress
cryptsetup close to-be-wiped
```


### Write SD-Card

Download image (take the lite version) from the [official page](https://www.raspberrypi.org/downloads/) and unzip it's content. Then write the Image to the Card:

```shell
dd bs=4M if=PATH_TO_IMG of=/dev/SD_CARD status=progress oflag=sync
```


### Headless Setup

WLAN: Edit `SD_CARD_ROOT/etc/wpa_supplicant/wpa_supplicant.conf` as following (with root privileges):

```
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=ch

network={
 ssid="SSID"
 psk="WLAN_PASSWORD"
}
```

Remark: Pleas make sure, that your 2.4GHz and 5GHz networks have different names, otherwise the Pi can't distinguish between the two.


### sshd

Create an `ssh` file in the boot partition (needs root privileges):

```shell
touch SD_CARD_BOOT/ssh
```

Default login (user/password): `pi/raspberry`.


## User

Connect via SSH to the Pi.

```shell
useradd temp
usermod -aG sudo temp
usermod -aG ssh temp
passwd temp
```

End SSH connection and login as _temp_ user.

```shell
pkill -u pi
usermod -l NEW_NAME pi
usermod -d /home/NEW_NAME -m NEW_NAME
groupmod -n NEW_NAME pi
```

End SSH connection and login as _NEW\_NAME_ user.

```shell
userdel -r -f temp
passwd
```

## Basic setup

Set the hostname:

```
hostname NEW_NAME
```

Get the configs:

```shell
apt install git
git clone https://gitlab.com/72th/my-config.git
my-config/setup/zsh-setup.sh
my-config/setup/02-prgms-term.sh
```

