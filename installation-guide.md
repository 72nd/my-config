# My Guide to Setup Stuff

## ZSH

Dependencies:

```shell script
sudo apt install zsh wget

sudo dnf install zsh wget
```

Oh my ZSH:

```shell script
RUNZSH=no sh -c "$(wget -O- https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```
