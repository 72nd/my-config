#!/bin/sh

sudo dnf install curl wget -y

# Install go
GO_ARCHIVE="$(curl https://go.dev/VERSION?m=text).linux-amd64.tar.gz"
wget "https://go.dev/dl/${GO_ARCHIVE}" -P /tmp
sudo tar -C /usr/local -xzf /tmp/${GO_ARCHIVE}
export PATH=$PATH:/usr/local/go/bin:$HOME/go/bin
rm /tmp/${GO_ARCHIVE}

# Install Task
go install github.com/go-task/task/v3/cmd/task@latest
