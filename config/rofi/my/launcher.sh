#!/usr/bin/env bash

# 72nd, based on work by Aditya Shakya

theme="style_1"
dir="$HOME/.config/rofi/my"

rofi -no-lazy-grab -show $1 -modi $1 -theme $dir/"$theme"
