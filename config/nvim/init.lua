-- Neovim configuration Version 2.0.11
-- By 72nd, 2019-2022

-- LEADER
vim.g.mapleader = ' '

-- PLUGINS
-- All plugins should have a comment describing my usage for them.
-- New plugins first enter a trial phase which has to be indicated
-- with '(TRIAL)'.
-- Last cleanup: 6. February 2022
require('packer').startup(function()
	-- Package manager
	use 'wbthomason/packer.nvim'
	-- Theme
	-- use 'navarasu/onedark.nvim'
	use 'ful1e5/onedark.nvim'
	-- Perserve the layout when deleting buffers
	use 'ojroques/nvim-bufdel'
	-- Statusline
	use {
		'nvim-lualine/lualine.nvim',
		requires = { 'kyazdani42/nvim-web-devicons', opt = true }
	}
	-- Fuzzy Finder
	use {
		'nvim-telescope/telescope.nvim',
		requires = { {'nvim-lua/plenary.nvim'} }
	}
	-- Sorter for Telescope
	use {'nvim-telescope/telescope-fzf-native.nvim', run = 'make' }
	-- Fast navigation (aka motion as they call it in here)
	use 'ggandor/lightspeed.nvim'
	-- Langauage Protocol Client configuration collection
	use 'neovim/nvim-lspconfig'
	-- Completion
	use {
		'ms-jpq/coq_nvim',
		branch = 'coq',
	}
	-- Treesitter interface for orgmode
	use 'nvim-treesitter/nvim-treesitter'
	-- Org mode, yay
	use 'nvim-orgmode/orgmode'
	-- Table Mode
	use 'dhruvasagar/vim-table-mode'
	-- Pandoc Markdown Syntax Highlighting 
	use 'vim-pandoc/vim-pandoc-syntax'
	-- use '~/repos/vim-pandoc-syntax/'
	-- Jinja2 template language syntx.
	use 'lepture/vim-jinja'
	-- use 'glench/vim-jinja2-syntax'
end)


-- GENERAL SETTINGS
vim.opt.shell = 'zsh'

-- Enable filetype based stuff
vim.cmd('filetype on')
vim.cmd('filetype plugin on')
vim.cmd('filetype plugin indent on')

-- Tabs
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.cmd('autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab')


-- APPEARANCE
vim.opt.termguicolors = true
vim.opt.number = true
vim.opt.mouse = 'a'

require('onedark').setup()


-- SPELL CHECKING
vim.opt.spelllang = 'de_de'
vim.opt.spellfile = '~/.config/nvim/de-de.utf-8.add'

-- Enable spell checking for markdown and org files
vim.cmd('autocmd BufRead,BufNewFile *.md setlocal spell')
vim.cmd('autocmd BufRead,BufNewFile *.org setlocal spell')

-- Statusline
local lualine_filename = require('lualine.components.filename'):extend()
local lualine_highlight = require('lualine.highlight')
local lualine_onedark = require('lualine.themes.onedark')

function lualine_filename:init(options)
	lualine_filename.super.init(self, options)
	self.options.path = 1
	local modified_highlight = lualine_highlight.create_component_highlight_group(
		{fg = '#e06c75'}, 'filename_status_modified', self.options
	)
	local readonly_highlight = lualine_highlight.create_component_highlight_group(
		{fg = '#e5c07b'}, 'filename_status_readonly', self.options
	)
	self.options.symbols.modified = lualine_highlight.component_format_highlight(modified_highlight) .. " "
	self.options.symbols.readonly = lualine_highlight.component_format_highlight(readonly_highlight) .. " "
end

require('lualine').setup {
	options = {
		icons_enabled = true,
		theme = 'onedark',
	},
	sections = {
		lualine_c = {
			{
				lualine_filename,
			},
		}
	}
}


-- LANGUAGE SERVER
local lsp = require "lspconfig"
local coq = require "coq"

lsp.pylsp.setup{}
lsp.pylsp.setup{coq.lsp_ensure_capabilities()}

lsp.rust_analyzer.setup{}

-- PLUGINS
-- Use jinja2-syntax for tera files
vim.cmd('autocmd BufNewFile,BufRead *.html.tera set syntax=jinja')

-- Telescope config
local actions = require("telescope.actions")
require('telescope').setup {
	defaults = {
		mappings = {
			i = {
				["<esc>"] = actions.close,
				["<C-j>"] = actions.move_selection_next,
				["<C-k>"] = actions.move_selection_previous,
			}
		}
	}
}
require('telescope').load_extension('fzf')

-- coq
vim.g.coq_settings = {
	['keymap.recommended'] = false,
	['keymap.jump_to_mark'] = '<A-i>',
	['keymap.bigger_preview'] = '<C-ö>',
}
vim.cmd('COQnow -s')

-- markdown
function markdown_settings()
	vim.opt.syntax = 'makrdown.pandoc'
	vim.opt.breakindentopt = 'shift:1,min:40,sbr'
	vim.opt.breakindent = true
	vim.opt.linebreak = true
end
vim.cmd('autocmd BufRead,BufNewFile *.md lua markdown_settings()')

-- Hack, until i understand pandoc-syntax correctly...
-- vim.cmd('autocmd BufRead,BufNewFile *todo.md match DiffAdded /TOMOROW/')
-- vim.cmd('autocmd BufRead,BufNewFile *todo.md 2match DiffChanged /TODAY/')
vim.cmd('autocmd BufRead,BufNewFile *todo.md syn match pandocTodoTomorrow /TOMORROW/ contained containedin=pandocListItem,pandocUListItem display')
vim.cmd('autocmd BufRead,BufNewFile *todo.md syn match pandocTodoToday /TODAY/ contained containedin=pandocListItem,pandocUListItem display')
vim.cmd('hi link pandocTodoTomorrow DiffAdded')
vim.cmd('hi link pandocTodoToday DiffChanged')

-- orgmode
local parser_config = require "nvim-treesitter.parsers".get_parser_configs()
parser_config.org = {
	install_info = {
		url = 'https://github.com/milisims/tree-sitter-org',
		revision = 'f110024d539e676f25b72b7c80b0fd43c34264ef',
		files = {'src/parser.c', 'src/scanner.cc'},
	},
	filetype = 'org',
}

require'nvim-treesitter.configs'.setup {
	highlight = {
		enable = true,
		disable = {'org'},
		additional_vim_regex_highlighting = {'org'},
	},
	ensure_installed = {'org'},
}

require('orgmode').setup({
	org_agenda_files = {'~/documents/org/*'},
	org_default_notes_file = '~/documents/org/default.org',
	org_indent_mode = 'noindent',
})

-- Handles the indention and wrapping for org mode files.
function org_indent()
	vim.opt.breakindent = true
	vim.opt.breakindentopt = 'shift:2,min:40,sbr'
	vim.opt.linebreak = true
end
vim.cmd('autocmd FileType org lua org_indent()')

-- ABBREVIATIONS
vim.cmd('iabbrev verb ```')
vim.cmd('iabbrev blb \\[...\\]')


-- CUSTOM METHODS

-- Use rsmooth to convert a markdown file to a PDF
function run_rsmooth()
	if vim.bo.filetype ~= 'markdown' then
		print('Aborted, this isn\'t a markdown file.')
		return
	end
	vim.cmd('write')
	local handle = io.popen('rsmooth ' .. vim.fn.expand('%') .. ' 2>&1')
	local result = handle:read("*a")
	if result ~= '' then
		local msg = string.gsub(result, '[ \t]+%f[\r\n%z]', '')
		print('⚠️  rsmooth error occured: ' .. msg)
	else 
		print('rsmooth done.')
	end
end

-- Open the corresponding PDF file to the current buffer
function open_pdf()
	if vim.bo.filetype ~= 'markdown' then
		print('Aborted, this isn\'t a markdown file.')
		return
	end
	local path = vim.fn.expand('%:r') .. '.pdf'
	if not vim.fn.filereadable(path) then
		print('⚠️  There isn\'t a pdf for this markdown file')
		return
	end
	io.popen('gio open ' .. path .. '< /dev/null &> /dev/null &')
end

-- Setup for editing a mail (Evolution external mode)
function mail_mode()
	vim.spell = true
	vim.opt.breakindentopt = 'shift:1,min:40,sbr'
	vim.opt.breakindent = true
	vim.opt.linebreak = true
end


--- KEY MAPPING AND REMAPPING
-- Window navigation
vim.api.nvim_set_keymap('n', '<Leader>w', '<C-w>', {noremap = true, silent=true})

-- Insert newline above in insert mode
vim.api.nvim_set_keymap('i', '<C-l>', '<C-o>O', {noremap = true, silent=true})

-- Free <C-k> for coq
vim.api.nvim_set_keymap('i', '<C-h>', '<C-k>', {noremap = true, silent=true})

-- Close buffer while perserve the window using the nvim-bufdel plugin
vim.api.nvim_set_keymap('n', '<Leader>q', '<Cmd>BufDel<CR>', { noremap = true, silent = true })

-- Remap system register paste
vim.api.nvim_set_keymap('', '<Leader>c', '"+', {noremap = true, silent=true})

-- Bind custom methods
vim.api.nvim_set_keymap('n', '<Leader>es', '<Cmd>lua run_rsmooth()<CR>', { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<Leader>eo', '<Cmd>lua open_pdf()<CR>', { noremap = true, silent = true })

-- Spell-check stuff
vim.api.nvim_set_keymap('n', 'z0', 'z=', {noremap = true})
vim.api.nvim_set_keymap('n', '<Leader>sd', '<Cmd>set spelllang=de_de<CR>', { noremap = true })
vim.api.nvim_set_keymap('n', '<Leader>se', '<Cmd>set spelllang=en_us<CR>', { noremap = true })

-- Map Telescope methods
vim.api.nvim_set_keymap('n', '<Leader>b', '<Cmd>lua require("telescope.builtin").buffers()<CR>', { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<Leader>ff', '<Cmd>lua require("telescope.builtin").find_files()<CR>', { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<Leader>fg', '<Cmd>lua require("telescope.builtin").live_grep()<CR>', { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<Leader>fh', '<Cmd>lua require("telescope.builtin").oldfiles()<CR>', { noremap = true, silent = true })

-- coq suggestion navigation
vim.cmd('ino <silent><expr> <C-j> pumvisible() ? "\\<C-n>" : "\\<Tab>"')  
vim.cmd('ino <silent><expr> <C-k> pumvisible() ? "\\<C-p>" : "\\<BS>"')  

-- Language Client
vim.api.nvim_set_keymap('n', 'K', '<Cmd>lua vim.lsp.buf.hover()<CR>', { noremap = true })
vim.api.nvim_set_keymap('n', 'gd', '<Cmd>lua require("telescope.builtin").lsp_definitions()<CR>', { noremap = true })
vim.api.nvim_set_keymap('n', '<Leader>ls', '<Cmd>lua require("telescope.builtin").lsp_document_symbols()<CR>', { noremap = true })
vim.api.nvim_set_keymap('n', '<Leader>lf', '<Cmd>lua vim.lsp.buf.formatting()<CR>', { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<Leader>lx', '<Cmd>lua require("telescope.builtin").lsp_references()<CR>', { noremap = true })
vim.api.nvim_set_keymap('n', '<Leader>lt', '<Cmd>lua require("telescope.builtin").lsp_type_definitions()<CR>', { noremap = true })
vim.api.nvim_set_keymap('n', '<Leader>la', '<Cmd>lua require("telescope.builtin").lsp_code_actions()<CR>', { noremap = true })
vim.api.nvim_set_keymap('n', '<Leader>ll', '<Cmd>lua require("telescope.builtin").lsp_range_code_actions()<CR>', { noremap = true })

-- Markdown: Rebind up/down keys for navigation in wrapped lines
vim.cmd('autocmd FileType markdown noremap <silent> j gj')
vim.cmd('autocmd FileType markdown noremap <silent> k gk')
